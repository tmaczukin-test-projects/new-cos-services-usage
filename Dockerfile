FROM golang:1.21.7-alpine3.19 AS builder

ENV CGO_ENABLED=0
WORKDIR /src
EXPOSE 1234

COPY . /src/

RUN go build service.go

FROM scratch

COPY --from=builder /src/service /usr/bin/service

ENTRYPOINT ["/usr/bin/service"]
CMD []
