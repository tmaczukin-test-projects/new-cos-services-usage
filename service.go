package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"syscall"
)

const (
	defaultListenAddr = ":1234"
)

func main() {
	listenAddr := os.Getenv("SERVICE_LISTEN_ADDR")
	if listenAddr == "" {
		listenAddr = defaultListenAddr
	}

	s := http.Server{
		Addr: listenAddr,
		Handler: http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
			rw.WriteHeader(http.StatusOK)
			rw.Header().Add("Content-Type", "text/plain")

			_, _ = fmt.Fprintf(rw, "Remote addr: %v\n", r.RemoteAddr)
		}),
	}

	ctx, cancelFn := signal.NotifyContext(context.Background(), syscall.SIGQUIT, syscall.SIGINT, syscall.SIGTERM)
	defer cancelFn()

	wg := new(sync.WaitGroup)
	wg.Add(1)

	go func() {
		defer wg.Done()

		fmt.Printf("Starting server listening on %s\n", s.Addr)
		err := s.ListenAndServe()
		if err == nil {
			fmt.Println("Server exited")
			return
		}

		fmt.Printf("Server exited with: %v\n", err)
	}()

	<-ctx.Done()

	err := s.Shutdown(context.Background())
	if err != nil {
		fmt.Printf("Shutdown error: %v\n", err)
	}

	wg.Wait()

	fmt.Println("All is done. Bye bye!")
}
